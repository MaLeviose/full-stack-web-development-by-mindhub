// document.getElementById("table‐header").innerHTML = getHeadersHtml(data)
// JSON.stringify(data, null, 2);

 fetch("https://api.propublica.org/congress/v1/116/House/members.json", {
     method: "GET", //estructura del JSON
     headers: new Headers({
         "X-API-Key": 'XZn2Z2asF5q5HN15s9sOqdmihriyNwPuaJKhOYmS'
     }),
 }).then(function (response) { //Una vez que termine esto hace esto
     if (response.ok)
         return response.json();
     throw new Error(response.statusText);
 }).then(function (json) {
    console.log(json);
     var members = json.results[0].members;
     mifuncion(members);
 });


 function mifuncion(miembros){
    var toHtml = "";
    for (let i = 0; i < miembros.length; i++) {
        let nombre = agarrarNombreCompleto(miembros[i].first_name, miembros[i].middle_name, miembros[i].last_name);
        let porcentaje = agarrarPorcentaje(miembros[i].votes_with_party_pct)
        toHtml = toHtml + "<tr>" +  nombre + siNoEsNull(miembros[i].party)+ siNoEsNull(miembros[i].state) + siNoEsNull(miembros[i].seniority) + porcentaje + "</tr>";
    }
    document.getElementById("table‐rows").innerHTML = toHtml; 
}

//** */
//Esto es sin fetch ******************************************************************************************/
//** */
// function getRowHtml(miembros) {
//     let todosLosRow = [];
//     for (let i = 0; i < miembros.length; i++) {
//         let nombre = agarrarNombreCompleto(miembros[i].first_name, miembros[i].middle_name, miembros[i].last_name);
//         let porcentaje = agarrarPorcentaje(miembros[i].votes_with_party_pct)
//         todosLosRow.push("<tr>" +  nombre + siNoEsNull(miembros[i].party)+ siNoEsNull(miembros[i].state) + siNoEsNull(miembros[i].seniority) + porcentaje + "</tr>");
//     }
//     return todosLosRow;
// }

 function siNoEsNull(valor){
    if (valor == null){
        return "<td>"+"</td>";
    }
    else{
        return "<td>" + valor + "</td>";
    }
}

function agarrarPorcentaje(numero) {
    if (numero == null)
        return "<td>"+"</td>";
    else
        return "<td>"+numero+"%"+"</td>";
}

function agarrarNombreCompleto(nombre, segundo, apellido){
    if(nombre != null && segundo != null && apellido !=null)
        return "<td>"+ apellido +", "+ nombre + " " + segundo +"</td>";
    else if (nombre != null && segundo != null && apellido ==null) 
        return "<td>"+ nombre + " " + segundo +"</td>";
    else if(segundo == null && nombre != null &&  apellido !=null)
        return "<td>"+ apellido +", "+ nombre +"</td>";
    else if(segundo != null && nombre == null &&  apellido !=null)
        return "<td>"+ apellido +", "+ segundo +"</td>";
    else if(segundo == null && nombre != null &&  apellido ==null)
        return "<td>"+ nombre +"</td>";
    else if(segundo == null && nombre == null &&  apellido !=null)
        return "<td>"+ apellido +"</td>";
    else if(segundo != null && nombre == null &&  apellido ==null)
        return "<td>"+ segundo +"</td>";
    else if (segundo == null && nombre == null &&  apellido ==null)
        return "<td>"+"</td>";
}

// function renderRow(data){
//     var html = getRowHtml(data.results[0].members);
//     document.getElementById("table‐rows").innerHTML = html.join("");
// }

// renderRow(data)
//*******************************************************************************************************/