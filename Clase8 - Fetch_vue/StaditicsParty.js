//Hago un js general y despues lo linkeo para Senate Attendance & Senate Party Loyalty con sus debidos id en el html

var objetoEstadistica = {
    "numberOf": {
        "democrats": 0,
        "republican": 0,
        "independent": 0
    },
    "averageVotesWithParty": {
        "democrats": 0.0,
        "republican": 0.0,
        "independent": 0.0
    },
}

const dataJson = data.results[0].members;

function sacarCant(miembros) {
    let democratas = 0;
    let republicanos = 0;
    let independientes = 0;
    for (let i = 0; i < miembros.length; i++) {
        if (miembros[i].party == "R")
            republicanos++;
        else if (miembros[i].party == "D")
            democratas++;
        else
            independientes++;
    }
    objetoEstadistica.numberOf.democrats = democratas;
    objetoEstadistica.numberOf.republican = republicanos;
    objetoEstadistica.numberOf.independent = independientes;
}

function promedioVotosCadaParty(miembros) {
    let sumaD = 0;
    let sumaR = 0;
    let sumaI = 0;

    for (let i = 0; i < miembros.length; i++) {
        if (miembros[i].votes_with_party_pct != undefined) {
            if (miembros[i].party == "R")
                sumaR = sumaR + miembros[i].votes_with_party_pct;
            else if (miembros[i].party == "D")
                sumaD = sumaD + miembros[i].votes_with_party_pct;
            else
                sumaI = sumaI + miembros[i].votes_with_party_pct;
        }
    }
    console.log("numero de republicanos " + objetoEstadistica.numberOf.republican + " suma de votos " + sumaR)
        //guardo solo 3 decimales con .toFixed(3)
    objetoEstadistica.averageVotesWithParty.democrats = (sumaD / objetoEstadistica.numberOf.democrats).toFixed(3);
    objetoEstadistica.averageVotesWithParty.republican = (sumaR / objetoEstadistica.numberOf.republican).toFixed(3);
    objetoEstadistica.averageVotesWithParty.independent = (sumaI / objetoEstadistica.numberOf.independent).toFixed(3);
}

//Testeando funciones
sacarCant(dataJson);
console.log("Cant democratas: " + objetoEstadistica.numberOf.democrats);
console.log("Cant independientes: " + objetoEstadistica.numberOf.independent);
console.log("Cant republicanos: " + objetoEstadistica.numberOf.republican);

promedioVotosCadaParty(dataJson);
console.log("Promedio democratas: " + objetoEstadistica.averageVotesWithParty.democrats);
console.log("Promedio independientes: " + objetoEstadistica.averageVotesWithParty.independent);
console.log("Promedio republicanos: " + objetoEstadistica.averageVotesWithParty.republican);


//Funciones party votes que saca los 10% mas altos y bajos
function copiarObj(object) {
    let copia = {};
    for (var key in object) {
        let value = object[key];
        //caso base: la cosa que estoy copiando es un objeto
        if (typeof(value) != 'object')
            copia[key] = value;

        //paso recursivo: para clonar objeto en segundo nivel o posterior
        else
            copia[key] = copiarObj(value);
    }
    return copia;
}

function menosVotados(dataJson) {
    let datos = copiarObj(dataJson); //****Complejidad: O(n) con n = cant de miembros
    let diezPorc = Math.floor(0.1 * dataJson.length);
    let res = [];
    while (res.length < diezPorc) { //****complejidad: O(k) k = es el porcentaje, asintoticamente O(n)
        //aca empiezo a buscar minimo
        let minimo = datos[0].votes_with_party_pct;
        let indice = 0;
        for (let i = 0; i < dataJson.length; i++) { //****busco minimo en O(n)
            if (!(datos[i].votes_with_party_pct == (-1)) && dataJson[i].votes_with_party_pct < minimo) {
                indice = i;
                minimo = datos[i].votes_with_party_pct;
            }
        }
        //en este estado encontre el minimo junto con su indice
        res.push(dataJson[indice]); //notar que pusheo dataJson que es mi objeto original!!
        datos[indice].votes_with_party_pct = (-1); //cambio mi objeto copia
    }
    return res;
}

function masVotados(dataJson) {
    let datos = copiarObj(dataJson); //****Complejidad: O(n) con n = cant de miembros
    let diezPorc = Math.floor(0.1 * dataJson.length);
    let res = [];
    while (res.length < diezPorc) { //****complejidad: O(k) k = es el porcentaje, asintoticamente O(n)
        //aca empiezo a buscar maximo
        let maximo = datos[0].votes_with_party_pct;
        let indice = 0;
        for (let i = 0; i < dataJson.length; i++) { //****busco maximo en O(n)
            if (!(datos[i].votes_with_party_pct == (-1)) && dataJson[i].votes_with_party_pct > maximo) {
                indice = i;
                maximo = datos[i].votes_with_party_pct;
            }
        }
        //en este estado encontre el minimo junto con su indice
        res.push(dataJson[indice]); //notar que pusheo dataJson que es mi objeto original!!
        datos[indice].votes_with_party_pct = (-1); //cambio mi objeto copia
    }
    return res;
}



//funciones para mostrar todo

function getRowTablaParty(miembros) { //Para generar fila de las tablas de estadisticas (ambas)
    let todosLosRow = [];
    for (let i = 0; i < miembros.length; i++) {
        let nombre = agarrarNombreCompleto(miembros[i].first_name, miembros[i].middle_name, miembros[i].last_name);
        let porcentaje = agarrarPorcentaje(miembros[i].votes_with_party_pct)
        todosLosRow.push("<tr>" + nombre + "<td>" + miembros[i].total_votes + "</td>" + porcentaje + "</tr>");
    }
    return todosLosRow;
}

function agarrarPorcentaje(numero) {
    if (numero == null)
        return "<td>" + "</td>";
    else
        return "<td>" + numero + "%" + "</td>";
}

function agarrarNombreCompleto(nombre, segundo, apellido) {
    if (nombre != null && segundo != null && apellido != null)
        return "<td>" + apellido + ", " + nombre + " " + segundo + "</td>";
    else if (nombre != null && segundo != null && apellido == null)
        return "<td>" + nombre + " " + segundo + "</td>";
    else if (segundo == null && nombre != null && apellido != null)
        return "<td>" + apellido + ", " + nombre + "</td>";
    else if (segundo != null && nombre == null && apellido != null)
        return "<td>" + apellido + ", " + segundo + "</td>";
    else if (segundo == null && nombre != null && apellido == null)
        return "<td>" + nombre + "</td>";
    else if (segundo == null && nombre == null && apellido != null)
        return "<td>" + apellido + "</td>";
    else if (segundo != null && nombre == null && apellido == null)
        return "<td>" + segundo + "</td>";
    else if (segundo == null && nombre == null && apellido == null)
        return "<td>" + "</td>";
}

let menosvotados = menosVotados(dataJson);
let masvotados = masVotados(dataJson);

function getRowtabla1(obj) {
    let todosLosRow = [];
    todosLosRow.push("<tr>" + "<td>" + "Republican" + "</td>" + "<td>" + obj.numberOf.republican + "</td>" + "<td>" + objetoEstadistica.averageVotesWithParty.republican + "%" + "</td>" + "</tr>");
    todosLosRow.push("<tr>" + "<td>" + "Democrat" + "</td>" + "<td>" + obj.numberOf.democrats + "</td>" + "<td>" + objetoEstadistica.averageVotesWithParty.democrats + "%" + "</td>" + "</tr>");
    todosLosRow.push("<tr>" + "<td>" + "Independent" + "</td>" + "<td>" + obj.numberOf.independent + "</td>" + "<td>" + objetoEstadistica.averageVotesWithParty.independent + "%" + "</td>" + "</tr>");
    return todosLosRow;
}

function rendertabla1(data) {
    let html = getRowtabla1(data);
    document.getElementById("table1").innerHTML = html.join("");
}
rendertabla1(objetoEstadistica);


function renderPartymenos(data) {
    let menos = getRowTablaParty(data);
    document.getElementById("menos").innerHTML = menos.join("");
}

function renderPartymas(data) {
    let menos = getRowTablaParty(data);
    document.getElementById("mas").innerHTML = menos.join("");
}

renderPartymas(masvotados)
renderPartymenos(menosvotados)